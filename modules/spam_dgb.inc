<?php

/**
 * @file
 * Include file for integration with guestbook entries.
 */

/**
 * Implementation of hook_dgb_entry().
 */
function guestbook_spam_dgb_entry(&$entry, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'update':
      spam_content_update($entry, 'dgb');
      break;

    case 'insert':
      spam_content_insert($entry, 'dgb');
      break;

    case 'delete':
      spam_content_delete($entry, 'dgb');
      guestbook_spam_delete($entry['id']);

      // We delete spam feedback messages if needed.
      if ($a3 == 'delete_feedback' && $entry['bid']) {
        guestbook_spam_feedback_delete(array(), $entry['bid']);
      }
      break;

    case 'form':
      if (is_array($entry['id'])) {
        _spam_dgb_eid($entry['id']['#value']);
      }
      break;

    case 'view':
      $entry['recipient_user'] = $a3;
      $entry['spam'] = FALSE;
      if (spam_score_is_spam(_spam_dgb_score($entry['id']))) {
        $entry['status'] = DGB_ENTRY_NOT_PUBLISHED;
        $entry['spam'] = TRUE;
      }
      break;
  }
}

/**
 * Implementation of hook_spamapi().
 */
function dgb_spamapi($op, $arg1 = NULL, $arg2 = NULL, $arg3 = NULL) {
  switch ($op) {
    case 'content_module':
      // Register with the spam api as a content type module.
      return 'dgb';

    case 'content_id':
      // Tell the spam module the id of a given guestbook entry.
      if (is_object($arg1)) {
        // The delete hook uses an object instead of an array.
        $arg1 = (array)$arg1;
      }
      return _spam_dgb_eid($arg1['id']);

    case 'content_types':
      // Register the "guestbook entry" content type with the spam module.
      return array(array(
        'name' => t('guestbook_entries'),
        'module' => t('Drupal Guestbook'),
        'title' => t('Drupal Guestbook entries'),
        'description' => t('Check this box to filter guestbook entries for spam.'),
        'default_value' => 1,
      ));

    case 'filter_content_type':
      return (variable_get('spam_filter_guestbook_entries', 1));

    case 'filter_fields':
      // Tell spam module which fields it should scan for spam.
      $fields['main'] = array('message');
      if (is_object($arg1)) {
        // The delete hook uses an object instead of an array.
        $arg1 = (array)$arg1;
      }
      if (isset($arg1['authorname'])) {
        $fields['other'][] = 'authorname';
      }
      if (isset($arg1['anonemail'])) {
        $fields['other'][] = 'anonemail';
      }
      if (isset($arg1['anonwebsite'])) {
        $fields['other'][] = 'anonwebsite';
      }
      return $fields;

    case 'feedback_form':
      $form = array();
      if (is_numeric($form['id'])) {
        $form['eid'] = array(
          '#type' => 'textfield',
          '#title' => t('Entry ID'),
          '#value' => $arg1['id'],
          '#disabled' => TRUE,
        );
      }
      // fall through...
    case 'error_form':
      if (!is_array($form)) {
        $form = array();
      }
      $form['message'] = array(
        '#type' => 'fieldset',
        '#title' => 'Message',
      );
      $form['message']['message'] = array(
        '#type' => 'textarea',
        '#title' => t('Guestbook entry'),
        '#value' => $arg1['message'],
        '#disabled' => TRUE,
      );
      $form['message']['author'] = array(
        '#type' => 'markup',
        '#prefix' => '<div><strong>' . t('Author') . ':</strong></div>',
        '#value' => theme('username', user_load(array('uid' => $arg1['author']))),
      );
      return $form;

    case 'load':
      return db_fetch_object(db_query('SELECT g.*, u.name AS authorname FROM {dgb} g LEFT JOIN {users} u ON u.uid = g.author WHERE g.id = %d', $arg1));

    case 'title':
      // Build a fake title from entry text.
      return db_result(db_query('SELECT LEFT(message, 64) FROM {dgb} WHERE id = %d', $arg1));

    case 'edit_link':
      $link = "guestbook/$arg1/edit";
      return $link;

    case 'status':
      $status = db_result(db_query('SELECT status FROM {dgb} WHERE id = %d', $arg1));
      if ($status == DGB_ENTRY_PUBLISHED) {
        return SPAM_PUBLISHED;
      }
      else {
        return SPAM_NOT_PUBLISHED;
      }
      break;

    case 'hostname':
      return db_result(db_query('SELECT hostname FROM {dgb} WHERE id = %d', $arg1));

    case 'link':
      /**
       * hook_link() can not be used. We use the hook_dgb_entry_link_alter().
       * @see guestbook_spam_dgb_entry_link_alter().
       */
      break;

    case 'redirect':
      $uid = db_result(db_query('SELECT recipient FROM {dgb} WHERE id = %d', $arg1));

      if ($uid) {
        return drupal_goto("user/$uid/dguestbook", NULL, "entry-$arg1");
      }
      else {
        return drupal_goto("site-guestbook", NULL, "entry-$arg1");
      }

    case 'overview_filter_join':
      return 'INNER JOIN {dgb} g ON t.content_id = g.id';

    case 'overview_filter_where':
      switch ($arg1) {
        case 'title':
          return "g.message LIKE '%%%s%%'";
        case 'status':
          return 'g.status = %d';
      }

    case 'menu':
      // Create page for listing all guestbook spam entries in guestbook admin section.
      $items = array();
      $items['admin/content/dgb/list/spam'] = array(
        'title' => t('Spam'),
        'page callback' => 'spam_dgb_admin',
        'access arguments' => array('administer spam'),
        'type' => MENU_LOCAL_TASK,
        'weight' => 10
      );
      return $items;

    case 'publish':
      // Only update guestbook entries that exist and need to be published and
      // need to be deleted exist spam feedback.
      $id = db_result(db_query('SELECT id FROM {dgb} WHERE id = %d AND status = %d OR status = %d', $arg1, DGB_ENTRY_NOT_PUBLISHED, SPAM_ENTRY));
      if ($id) {
        db_query('UPDATE {dgb} SET status = %d WHERE id = %d', DGB_ENTRY_PUBLISHED, $arg1);

        guestbook_spam_feedback_delete(array($arg1 => $arg1));
      }
      break;

    case 'unpublish':
      // Only update guestbook entries that exist and need to be unpublished.
      // When 'unpublishing' guestbook entries, we actually mark them as spam.
      $id = db_result(db_query('SELECT id FROM {dgb} WHERE id = %d AND status = %d', $arg1, DGB_ENTRY_PUBLISHED));
      if ($id) {
        db_query('UPDATE {dgb} SET status = %d WHERE id = %d', SPAM_ENTRY, $arg1);
      }
      break;

    case 'theme_forms':
      // Add spam guestbook admin form to spam.module's hook_theme.
      return array(
        'spam_dgb_admin_overview' => array(
          'file' => 'modules/spam_dgb.inc',
          'arguments' => array(
            'form' => NULL
          )
        )
      );
  }
}

/**
 * Implementation of hook_dgb_entry_link_alter().
 *
 * - Add the spam module links to the the guestbook entry links and
 *   reorganize the links to display the spam module links in penultimate position.
 * - Usage of teh module function guestbook_spam_spam_links() to use the
 *   additional permission 'administer own guestbook spam'.
 */
function guestbook_spam_dgb_entry_link_alter($links, $entry) {
  $get_links = guestbook_spam_spam_links('dgb', $entry['id'], (object)$entry);

  foreach ($get_links as $key => $val) {
    $spam_links[$key] = $val;
  }

  // Reorganize the links.
  array_splice($links, count($links)-1, 0, $spam_links);
}

/**
 * Form alter gets its own function so we can reference &$form without causing
 * errors in PHP4 installations.  (If we use spamapi, we have to set a default,
 * which PHP4 doesn't support.)
 */
function dgb_spamapi_form_alter(&$form, &$form_state, $form_id) {
  // Make it possible to delete spam feedback.
  if ($form_id == 'dgb_delete_entry_confirm') {
    $form['#submit'][] = 'guestbook_spam_delete_submit';
  }
  if ($form_id == 'dgb_multiple_delete_confirm') {
    $form['#submit'][] = 'guestbook_spam_multiple_delete_submit';
  }

  // Scan guestbook entries before they are inserted into the database.
  if ($form_id == 'dgb_form_entry_form' && is_array($form)) {
    $form['#validate'][] = 'guestbook_spam_scan';
  }
  // Add spam options to guestbook entry form.
  else if ($form_id == 'dgb_entry_admin_overview' || $form_id == 'spam_dgb_admin_overview') {
    $form['#spam_submit_valuetype'] = 'guestbook_entries';
    $form['#spam_submit_itemtype'] = 'dgb';

    if ($form_id == 'dgb_entry_admin_overview') {
      $form['#submit'] = array_merge(array('spam_dgb_admin_overview_submit'), $form['#submit']);
    }

    $parameters = $form['#parameters'];
    if (is_array($parameters)) {
      if (!in_array('new', $parameters)) {
        $form['options']['operation']['#options']['markasnotspam'] = t('Mark the selected guestbook entries as not spam');
      }
      if (!in_array('spam', $parameters)) {
        $form['options']['operation']['#options']['markasspam'] = t('Mark the selected guestbook entries as spam');
      }
      if (in_array('new', $parameters)) {
        $form['options']['operation']['#options']['teachnotspam'] = t('Teach filters selected guestbook entries are not spam');
      }
    }
    if (is_array($form_state['post']) && is_array($form_state['post']['guestbook_entries'])) {
      foreach ($form_state['post']['guestbook_entries'] as $eid) {
        if ($form_state['post']['operation'] == 'markasspam') {
          $score = _spam_dgb_score($eid);
          if (!spam_score_is_spam($score)) {
            spam_mark_as_spam('dgb', $eid);
          }
        }
        else if ($form_state['post']['operation'] == 'markasnotspam') {
          $score = _spam_dgb_score($eid);
          if (spam_score_is_spam($score)) {
            spam_mark_as_not_spam('dgb', $eid);
          }
        }
        else if ($form_state['post']['operation'] == 'teachnotspam') {
          spam_mark_as_not_spam('dgb', $eid);
        }
      }
    }
  }
}

/**
 * Validate spam_dgb_admin_overview form submissions.
 *
 * We can't execute any 'Update options' if no entries were selected.
 */
function spam_dgb_admin_overview_validate($form, &$form_state) {
  $form_state['values']['guestbook_entries'] = array_diff($form_state['values']['guestbook_entries'], array(0));
  if (count($form_state['values']['guestbook_entries']) == 0) {
    form_set_error('', t('Please select one or more entries to perform the update on.'));
  }
}

/**
 * Submit callback to realize the spam mark and spam unmark ations.
 * 
 * @see dgb_spamapi_form_alter()
 */
function spam_dgb_admin_overview_submit($form, &$form_state) {
  spam_admin_overview_submit($form, &$form_state);
}

/**
 * Retreive spam score, caching in memory for repeated use.
 */
function _spam_dgb_score($eid) {
  static $scores = array();

  if (!isset($scores[$eid])) {
    $scores[$eid] = db_result(db_query("SELECT score FROM {spam_tracker} WHERE content_type = 'dgb' AND content_id = %d", $eid));
  }

  return $scores[$eid];
}

/**
 * Menu callback; present an administrative spam guestbook entry listing.
 */
function spam_dgb_admin() {
  $edit = $_POST;

  if ($edit['operation'] == 'delete' && $edit['guestbook_entries']) {
    module_load_include('inc', 'dgb', '/inc/dgb.admin');
    return drupal_get_form('dgb_multiple_delete_confirm');
  }
  else {
    drupal_set_title(t('Guestbook spam'));
    return drupal_get_form('spam_dgb_admin_overview', arg(4));
  }
}

/**
 * The 5.x version of guestbook_admin_overview doesn't provide hooks to easily
 * create a spam overview page.  This is nearly an exact copy of that function,
 * with minor changes for loading spam and processing it.
 */
function spam_dgb_admin_overview(&$form_state, $arg) {
  $destination = drupal_get_destination();

  // Build an 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset', '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>'
  );

  $options = array(
    'markasnotspam' => t('Mark the selected guestbook entries as not spam'),
    'delete' => t('Delete the selected guestbook entries')
  );

  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'markasnotspam'
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update')
  );

  $form['header'] = array('#type' => 'value', '#value' => array(
    theme('table_select_header_cell'),
    array('data' => t('Entry'), 'field' => 'subject'),
    array('data' => t('Author'), 'field' => 'author'),
    array('data' => t('Guestbook'), 'field' => 'recipient'),
    array('data' => t('Time'), 'field' => 'created', 'sort' => 'desc'),
    array('data' => t('Operations'))
  ));
  
  // Load the guestbook entries that we want to display.
  $result = pager_query('SELECT g.id AS eid, g.recipient, g.author, g.anonname, g.anonemail, g.anonwebsite, g.message, g.created, g.status, u.name AS registered_name, u.uid, u2.name AS recipient_name
                         FROM {dgb} g
                         INNER JOIN {users} u ON u.uid = g.author
                         LEFT JOIN {users} u2 ON u2.uid = g.recipient
                         WHERE g.status = %d'. tablesort_sql($form['header']['#value']), 50, 0, NULL, SPAM_ENTRY);

  // Build a table listing the appropriate guestbook entries.
  while ($entry = db_fetch_object($result)) {
    $guestbook_entries[$entry->eid] = '';
    $subject = filter_xss($entry->message);
    
    // Make sure the use of name from anonymous poster.
    $entry->name = $entry->author ? $entry->registered_name : $entry->anonname;
    // Make sure the different links to site and user guestbooks.
    if ($entry->recipient > 0) {
      // Fake an account object.
      $recipient = new stdClass();
      $recipient->uid = $entry->recipient;
      $recipient->name = $entry->recipient_name;
      $guestbook = theme('username', $recipient);
    }
    else {
      $guestbook = l(variable_get('dgb_site_title', t('Site guestbook')), dgb_dgb_path($entry->recipient));
    }

    $form['subject'][$entry->eid] = array('#value' => l(truncate_utf8($subject, 32), drupal_get_path_alias(dgb_dgb_path($entry->recipient)), array('attributes' => array('title' => truncate_utf8($subject, 256)), 'fragment' => 'entry-'. $entry->eid)));
    $form['username'][$entry->eid] = array('#value' => theme('username', $entry));
    $form['recipient'][$entry->eid] = array('#value' => $guestbook);
    $form['created'][$entry->eid] = array('#value' => format_date($entry->created, 'small'));
    $form['operations'][$entry->eid] = array('#value' => l(t('edit'), dgb_dgb_path($entry->recipient) .'/edit/'. $entry->eid, array('query' => $destination)));
  }

  $form['guestbook_entries'] = array(
    '#type' => 'checkboxes',
    '#options' => $guestbook_entries
  );
  $form['pager'] = array(
    '#value' => theme('pager', NULL, 50, 0)
  );

  return $form;
}

/**
 * Display the admin overview of spam guestbook entries.
 *
 * @param array $form
 * @return string
 * @ingroup themeable
 */
function theme_spam_dgb_admin_overview($form) {
  $output = drupal_render($form['options']);

  if (isset($form['subject']) && is_array($form['subject'])) {
    foreach (element_children($form['subject']) as $key) {
      $row = array();
      $row[] = drupal_render($form['guestbook_entries'][$key]);
      $row[] = drupal_render($form['subject'][$key]);
      $row[] = drupal_render($form['username'][$key]);
      $row[] = drupal_render($form['recipient'][$key]);
      $row[] = drupal_render($form['created'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No guestbook entries available.'), 'colspan' => '6'));
  }

  $output .= theme('table', $form['header']['#value'], $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Cache the guestbook entry id to be sure it's available when we need it.
 *
 * @param int $id
 *   A guestbook entry ID.
 * @return int
 *   A guestbook entry ID.
 */
function _spam_dgb_eid($id = NULL) {
  // $eid; entry ID
  static $eid = 0;

  if (isset($id)) {
    $eid = (int)$id;
  }

  return $eid;
}

/**
 * Scan guestbook entry content before it is posted into the database.
 */
function guestbook_spam_scan($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    $entry = $form['#post'];
    $_SESSION['spam_form'] = $form;
    spam_scan($entry, 'dgb');
  }
  else if (isset($_SESSION['spam_form'])) {
    unset($_SESSION['spam_form']);
  }
}
