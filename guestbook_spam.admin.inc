<?php

/**
 * @file
 * Administer Drupal Guestbook Spam.
 */

function guestbook_spam_admin() {
  $form = array();

  $spam_entry_counter = _guestbook_spam_entry_load_spam(TRUE);
  $destination = drupal_get_destination();
  drupal_set_title(t('Guestbook spam settings'));

  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron'),
    '#description' => t('Use cron to manage spam entries.'),
    '#collapsible' => false,
    '#collapsed' => false
  );
  $value_spam_entries = t('Identified stored spam entries: !count', array('!count' => $spam_entry_counter));
  $form['cron']['spam_entry_info'] = array(
    '#type'  => 'markup',
    '#value' => $value_spam_entries,
  );
  $form['cron']['guestbook_spam_cron'] = array(
    '#type' => 'select',
    '#title' => 'Use cron',
    '#options' => array(
      0 => t('Do not use cron'),
      1 => t('Use cron to delete entries without feedback'),
      2 => t('Use cron to delete all stored spam entries')
    ),
    '#description' => t('Use this options to delete stored guestbook entries with cron are marked as spam.'),
    '#default_value' => variable_get('guestbook_spam_cron', 0)
  );

  if ($spam_entry_counter > 0) {
    $form['cron']['run'] = array(
      '#type' => 'fieldset',
      '#title' => t('Run cron'),
      '#collapsible' => false,
      '#collapsed' => false
    );
    $value_cron_run = t('You can <a href="@cron">run cron manually</a>.', array(
      '@cron' => url('admin/reports/status/run-cron', array('query' => drupal_get_destination())))
    );
    $form['cron']['run']['cron_run'] = array(
      '#type'  => 'markup',
      '#value' => $value_cron_run,
    );
  }

  return system_settings_form($form);
}
