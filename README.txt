
Drupal Guestbook Spam provides Spam module integration for
the Drupal Guestbook (DGB) module.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.

- The Drupal Guestbook (DGB) module.
- The Spam module.

Installation
--------------------------------------------------------------------------------
Copy the Drupal Guestbook Spam module folder to your module directory and then
enable on the admin modules page.

Administration
--------------------------------------------------------------------------------
1. Configure the module permissions.

   The permission 'administer own guestbook spam' allows:
   - guestbook owners can manage in their guestbook
     all entries as spam or not spam.
   - A registered user as entry author can manage in all guestbooks
     their entries as spam or not spam.

   This is useful if the Spam module configured to store spam entries.

2. Administer the module settings in: admin/settings/dgb/spam-settings

3 a. Administer the DGB content in: admin/content/dgb
     This section allows to manage spam entries.
     Please take a look at the Spam module administration in admin/settings/spam
     and see the options in Actions -> Posting actions.

3 b. Administer guestbook spam content in: admin/content/spam

Usage
--------------------------------------------------------------------------------
The module allows to delete stored spam entries with cron.

Don't forget to take a look at the spam logs.

Documentation
--------------------------------------------------------------------------------
Please use Drupals Help module to read the Drupal Guestbook Spam documentation.

